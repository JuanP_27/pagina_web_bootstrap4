$(function () {
    $("[data-toogle = 'tooltip']").tooltip();
    $("[data-toogle = 'popover']").popover();
    $('.carousel').carousel({
        interval: 3000
    });
    $('#contacto').on('show.bs.modal', function (e) {
        console.log('El modal contacto se está mostrando');
        $('#btnContactanos').removeClass('btn-outline-success');
        $('#btnContactanos').addClass('btn-outline-primary');
        $('#btnContactanos').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e) {
        console.log('El modal contacto se mostró');
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log('El modal contacto se está ocultando');
        $('#btnContactanos').removeClass('btn-outline-primary');
        $('#btnContactanos').addClass('btn-outline-success');
        $('#btnContactanos').prop('disabled', false);
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log('El modal contacto se ocultó');
    });
});